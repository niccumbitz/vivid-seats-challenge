var app = angular.module('VividSeats', ['ui.router', 'ui.bootstrap', 'ui.bootstrap.datetimepicker']);

app.config(function($stateProvider, $urlRouterProvider)
{
    $urlRouterProvider.otherwise('/all');
    
    $stateProvider
	    .state('all', {
	        url: "/all",
	        templateUrl: "partials/allEventsTable.html"
	    })
	    .state('upcoming', {
	        url: "/upcoming",
	        templateUrl: "partials/upcomingEventsTable.html"
	    })
	    .state('local', {
	        url: "/local",
	        templateUrl: "partials/localEventsTable.html"
	    });
})

app.filter("dateFilter", function() {
	return function(items, direction) {
        var currentDate = new Date().getTime();
        var arrayToReturn = [];

        if (items)
	        for (var i=0; i<items.length; i++){
	            var eventDate = new Date(items[i].date).getTime();

	            if (direction == 'previous' && currentDate >= eventDate) {
	            	arrayToReturn.push(items[i]);
	            } else if (currentDate < eventDate) {
	            	arrayToReturn.push(items[i]);
	            }
	        }
        
        return arrayToReturn;
	};
});