app.controller('EventFormController', function($scope, $uibModalInstance, EventService, editableEventData)
{
	// Form Constants
	$scope.dateOptions = {
		showWeeks: false,
		startingDay: 0
	};
  
	$scope.open = function($event,opened) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.dateOpened = true;
	};
  
	$scope.dateOpened = false;
	$scope.hourStep = 1;
	$scope.format = "dd-MMM-yyyy";
	$scope.minuteStep = 15;


	// if editableEventData is set then use that data otherwise set object to id: 0 
	$scope.eventData = editableEventData ? editableEventData : {id: 0}

	$scope.editableEventData = angular.copy($scope.eventData);


	// Form Options
	// on Form Submit
	$scope.submitForm = function(data)
	{
		// Check if form is valid
		if (this.eventForm.$invalid) {
			// Trigger error validation
			alert('please fill in the whole form');
			return false;
		} else {
			// Change date to JSON
			$scope.editableEventData.date = $scope.editableEventData.date.toJSON();
		}


		if ($scope.editableEventData.id == 0) //Event is being added
		{
			// Generate random 3 digit id
			// Would normally be response key from database
			$scope.editableEventData.id = Math.floor(Math.random()*899) + 100;
			// Add event via the service
			EventService.add(
				$scope.editableEventData,
				function(){
					// log that event was successfully added
					console.log('create successful');

					$scope.eventData = angular.copy($scope.editableEventData);
					$uibModalInstance.close($scope.eventData);
				}, function(error){
					// Handle Error
					console.error('create failed');

					alert(error+': Please try again');
				});
		} else {
			// Update event via the service
			EventService.update(
				$scope.editableEventData,
				function(){
					// Handle Success
					console.log('update successful');

					$scope.eventData = angular.copy($scope.editableEventData);
					$uibModalInstance.close($scope.eventData);

				}, function(){
					// Handle Error
					console.error('update failed');

					alert(error+': Please try again');
				});	
		}		
	}

	// on Form Cancel
	$scope.cancelForm = function()
	{
		// Dismiss the modal
		$uibModalInstance.dismiss('cancel');
	};
});