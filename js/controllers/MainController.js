app.controller('MainController', ['$scope', 'EventService', '$filter', '$uibModal', '$location', '$state', function($scope, EventService, $filter, $uibModal, $location, $state)
{
	// Static scope variables	
	$scope.title = 'Vivid Seats';
	$scope.local = 'Chicago'; // Assume local is equal to Chicago
	$scope.toastAlerts = [];
	$scope.tabs = [
        { heading: "All Events", route:"all", active:false },
        { heading: "Upcoming Events", route:"upcoming", active:false },
        { heading: "Local Events", route:"local", active:false },
    ];


	// Gets all events
    $scope.getEvents = function()
    {
    	EventService.all(function(data) {
    		$scope.$apply(function(){
    			// Update Events 
    			$scope.events = data;	
    		});
    	}, function(error){
			alert(error);
		});
    };

    // Filter Events Array to get the event with the selected Id
    $scope.getEvent = function(eventId)
    {
		var event = $filter('filter')($scope.events, {id: eventId}, true);
		return event.length ? event[0] : 'Not Found';
    };

    // Event to delete the function
   	$scope.deleteEvent = function(eventId){
    	if (confirm('This will permanently remove the event'))
    		EventService.remove(
    			// get event to remove
    			$scope.getEvent(eventId),
				function(){
					$scope.$apply(function(){
		    			// Remove event from events array
						$scope.events = $scope.events.filter(function(item){return item.id !== eventId});
						$scope.toastMessage({ type: 'success', msg: 'Event successfully removed' });
		    		});
				},
				function(){
					// Console an error
					console.error('Issue when attempting to remove an event');
					$scope.$apply(function(){
						// Good place to warn the user that there was an issue
						$scope.toastMessage({ type: 'danger', msg: 'Error occured while trying to delete the event' });
					});
				});	
    };


    // Modal forms for events
    // Add Modal Form
    $scope.openNewEventForm = function(){
    	$uibModal.open({
    		templateUrl: '/eventFormTemplate.html',
      		controller: 'EventFormController',
      		resolve: {
		        editableEventData: function () {
		        	// Return empty object
					return undefined;
		        }
			}
    	}).result.then(function(data){
			// Get Updated Events
			// Push new Event to events array;
			$scope.events = $scope.events.push(data)
			$scope.toastMessage({ type: 'success', msg:data.name + ' @' + data.venue.name + ' has been successfully added' });
    	}).catch(function(error)
    	{
    		// refrain from adding the event
    		if (error === 'cancel') {
    			// do nothing as user canceled the modal
    		}
    		else {
    			$scope.toastMessage({ type: 'danger', msg: 'Error occured while trying to addingf the event' });	
    		}
    	});
    };

    // Edit Modal Form
    $scope.openEditEventForm = function(eventId){
    	$uibModal.open({
    		templateUrl: '/eventFormTemplate.html',
      		controller: 'EventFormController',
			resolve: {
				editableEventData: function () {
					return $scope.getEvent(eventId);
				}
			}
    	}).result.then(function(data)
    	{
    		// Get Updated Events
    		// Mimicing service call so I can avoid a server request by updating the scope locally instead of making a new call to getEvents
    		$scope.events.forEach(function(event){
				if(data.id === event.id)
				{
					Object.getOwnPropertyNames(data).forEach(function(name){
						var desc = Object.getOwnPropertyDescriptor(data,name);
						Object.defineProperty(event, name, desc);
					});
				}
		    });

			$scope.toastMessage({ type: 'success', msg:data.name + ' @' + data.venue.name + ' has been successfully updated' });
    	}).catch(function(error)
    	{
    		// refrain from updating the event
    		if (error === 'cancel') {
    			// do nothing as user canceled the modal
    		}
    		else {
    			$scope.toastMessage({ type: 'danger', msg: 'Error occured while trying to update the event' });	
    		}
    	});
    };

    // Order events by date
    $scope.sortEvents = function(event){
    	var date = new Date(event.date).getTime();
	    return date;
    };


    $scope.toastMessage = function(messageObj){
    	$scope.toastAlerts.push(messageObj);
    };

    $scope.closeAlert = function(index) {
    	$scope.toastAlerts.splice(index, 1);
  	};

  	$scope.selectedIndex = 0;

    $scope.go = function(route){
        $state.go(route);
    };
 
    $scope.active = function(route){
        return $state.is(route);
    };
 
    $scope.$on("$stateChangeSuccess", function() {
        $scope.tabs.forEach(function(tab) {
            tab.active = $scope.active(tab.route);
        });
    });


    $scope.getEvents();
}]);